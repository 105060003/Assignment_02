var menuState = {
    create: function(){
         
        this.background = game.add.image(0, 0, 'menu-background'); 
        this.background.scale.setTo(0.5,0.8);
        
        var NameLabel = game.add.text(game.width/2,100,'Radien',{font:'100px Baloo Bhai',fill:'#ffffff'});
        NameLabel.anchor.setTo(0.5,0.5);
        //Explain how to start the game

        this.ChooseLabel = game.add.text(100,215,'>',{font:'40px Monda',fill:'#ffffff'});
        this.ChooseLabel.anchor.setTo(0.5,0.5);

        this.PlayLabel = game.add.text(game.width/2,220,'Play 1P',{font:'35px Monda',fill:'#ffffff'});
        this.PlayLabel.anchor.setTo(0.5,0.5);
        this.PlayLabel2 = game.add.text(game.width/2,280,'Play 2P',{font:'35px Monda',fill:'#ffffff'});
        this.PlayLabel2.anchor.setTo(0.5,0.5);
        this.SettingLabel = game.add.text(game.width/2,340,'Setting',{font:'35px Monda',fill:'#ffffff'});
        this.SettingLabel.anchor.setTo(0.5,0.5);
        this.LeaderBoardLabel = game.add.text(game.width/2,400,'Leader Board',{font:'35px Monda',fill:'#ffffff'});
        this.LeaderBoardLabel.anchor.setTo(0.5,0.5);

        var startLabel = game.add.text(game.width/2,game.height-80,'press " Enter " to choose',{font:'30px Monda',fill:'#ffffff'});
        startLabel.anchor.setTo(0.5,0.5);
        game.time.events.loop(800,
            function(){
                if(startLabel.alpha == 0)
                startLabel.alpha  = 1;
                else
                startLabel.alpha  = 0;
            }
            ,this); //to enable enemies every few seconds
        
        var EnterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        EnterKey.onDown.add(this.start,this);
        //var UPKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        //UPKey.onDown.add(this.keydown,this);
        var DOWNKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        DOWNKey.onDown.add(this.keydown,this);
        var UPKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        UPKey.onDown.add(this.keydown2,this);

        this.which = 1; //1: play 1P 2: play 2P 3:setting 4:leaderboard 

        //console.log( this.SettingLabel.style.font);
        //console.log( this.PlayLabel.style.font);
        console.log("volume = " + game.global.volume );
        console.log("bgm = " + game.global.bgm );
        console.log("SE = " + game.global.sound_effect);

    },
    update: function(){
        if(this.ChooseLabel.y == 215){
            this.which = 1;
            this.SettingLabel.scale.setTo(1,1);
            this.PlayLabel.scale.setTo(1.1,1.1);
            this.PlayLabel2.scale.setTo(1,1);
            this.LeaderBoardLabel.scale.setTo(1,1);
            //console.log( this.SettingLabel.style.font);
        //console.log( this.PlayLabel.style.font);
            
        }
        else if(this.ChooseLabel.y ==275){
            this.which = 2;
            this.SettingLabel.scale.setTo(1,1);
            this.PlayLabel.scale.setTo(1,1);
            this.PlayLabel2.scale.setTo(1.1,1.1);
            this.LeaderBoardLabel.scale.setTo(1,1);
        }
        else if(this.ChooseLabel.y ==335){
            this.which = 3;
            this.SettingLabel.scale.setTo(1.1,1.1);
            this.PlayLabel.scale.setTo(1,1);
            this.PlayLabel2.scale.setTo(1,1);
            this.LeaderBoardLabel.scale.setTo(1,1);
        }
        else{
            this.which = 4;
            this.SettingLabel.scale.setTo(1,1);
            this.PlayLabel.scale.setTo(1,1);
            this.PlayLabel2.scale.setTo(1,1);
            this.LeaderBoardLabel.scale.setTo(1.1,1.1);
        }
    },
    keydown: function(){
        if(this.ChooseLabel.y == 215)
            this.ChooseLabel.y = 275;
        else if(this.ChooseLabel.y == 275)
            this.ChooseLabel.y = 335;
        else if(this.ChooseLabel.y == 335)
            this.ChooseLabel.y = 395;
        else
            this.ChooseLabel.y = 215;

        
    },
    keydown2: function(){
        if(this.ChooseLabel.y == 395)
            this.ChooseLabel.y = 335;
        else if(this.ChooseLabel.y == 335)
            this.ChooseLabel.y = 275;
        else if(this.ChooseLabel.y == 275)
            this.ChooseLabel.y = 215;
        else
            this.ChooseLabel.y = 395;

        
    },
    start: function(){
        //Start the actual game
        if(this.which == 1)
            game.state.start('play');
        else if(this.which == 2)
            game.state.start('play2');  
        else if(this.which == 3)
        {
            black = game.add.sprite(0,0,'black');
            black.alpha = 0.9;
            this.bgmText = game.add.text(300,200,'BGM',{font:'40px Maiden Orange',fill:'#ffffff'});
            this.bgmText.anchor.setTo(0.5,0.5);
            this.SEText = game.add.text(350,300,'Sound Effect',{font:'40px Maiden Orange',fill:'#ffffff'});
            this.SEText.anchor.setTo(0.5,0.5);
            this.volumeText = game.add.text(320,400,'Volume',{font:'40px Maiden Orange',fill:'#ffffff'});
            this.volumeText.anchor.setTo(0.5,0.5);
            this.add = this.game.add.sprite(220, 381, 'add');
            this.add.scale.setTo(0.5,0.5);
            this.add.inputEnabled = true;
            this.add.events.onInputUp.add(function () {
                if(game.global.volume == 0.0){
                    game.global.volume = 0.1;
                    console.log("volume = " + game.global.volume );
                    this.volumeText.text = game.global.volume*10;
                }
                else if(game.global.volume == 0.1){
                    game.global.volume = 0.2;
                    console.log("volume = " + game.global.volume );
                    this.volumeText.text = game.global.volume*10;
                }
                else if(game.global.volume == 0.2){
                    game.global.volume = 0.3;
                    console.log("volume = " + game.global.volume );
                    this.volumeText.text = game.global.volume*10;
                }
                else if(game.global.volume == 0.3){
                    game.global.volume = 0.4;
                    console.log("volume = " + game.global.volume );
                    this.volumeText.text = game.global.volume*10;
                }
                else if(game.global.volume == 0.4){
                    game.global.volume = 0.5;
                    console.log("volume = " + game.global.volume );
                    this.volumeText.text = game.global.volume*10;
                }
                else if(game.global.volume == 0.5){
                    game.global.volume = 0.6;
                    console.log("volume = " + game.global.volume );
                    this.volumeText.text = game.global.volume*10;
                }
                else if(game.global.volume == 0.6){
                    game.global.volume = 0.7;
                    console.log("volume = " + game.global.volume );
                    this.volumeText.text = game.global.volume*10;
                }
                else if(game.global.volume == 0.7){
                    game.global.volume = 0.8;
                    console.log("volume = " + game.global.volume );
                    this.volumeText.text = game.global.volume*10;
                }
                else if(game.global.volume == 0.8){
                    game.global.volume = 0.9;
                    console.log("volume = " + game.global.volume );
                    this.volumeText.text = game.global.volume*10;
                }
                else if(game.global.volume == 0.9){
                    game.global.volume = 1.0;
                    console.log("volume = " + game.global.volume );
                    this.volumeText.text = game.global.volume*10;
                }
            
            }, this);
            
            this.subtract = this.game.add.sprite(120, 381, 'subtract');
            this.subtract.scale.setTo(0.5,0.5);
            this.subtract.inputEnabled = true;
            this.subtract.events.onInputUp.add(function () {
                if(game.global.volume == 0.1){
                    game.global.volume = 0.0;
                    console.log("volume = " + game.global.volume );
                    this.volumeText.text = game.global.volume*10;
                }
                else if(game.global.volume == 0.2){
                    game.global.volume = 0.1;
                    console.log("volume = " + game.global.volume );
                    this.volumeText.text = game.global.volume*10;
                }
                else if(game.global.volume == 0.3){
                    game.global.volume = 0.2;
                    console.log("volume = " + game.global.volume );
                    this.volumeText.text = game.global.volume*10;
                }
                else if(game.global.volume == 0.4){
                    game.global.volume = 0.3;
                    console.log("volume = " + game.global.volume );
                    this.volumeText.text = game.global.volume*10;
                }
                else if(game.global.volume == 0.5){
                    game.global.volume = 0.4;
                    console.log("volume = " + game.global.volume );
                    this.volumeText.text = game.global.volume*10;
                }
                else if(game.global.volume == 0.6){
                    game.global.volume = 0.5;
                    console.log("volume = " + game.global.volume );
                    this.volumeText.text = game.global.volume*10;
                }
                else if(game.global.volume == 0.7){
                    game.global.volume = 0.6;
                    console.log("volume = " + game.global.volume );
                    this.volumeText.text = game.global.volume*10;
                }
                else if(game.global.volume == 0.8){
                    game.global.volume = 0.7;
                    console.log("volume = " + game.global.volume );
                    this.volumeText.text = game.global.volume*10;
                }
                else if(game.global.volume == 0.9){
                    game.global.volume = 0.8;
                    console.log("volume = " + game.global.volume );
                    this.volumeText.text = game.global.volume*10;
                }
                else if(game.global.volume == 1.0){
                    game.global.volume = 0.9;
                    console.log("volume = " + game.global.volume );
                    this.volumeText.text = game.global.volume*10;
                }
            
            }, this);

            this.volumeText = game.add.text(185,400,game.global.volume*10,{font:'40px Maiden Orange',fill:'#ffffff'});
            this.volumeText.anchor.setTo(0.5,0.5);

            this.soundeffect_check = this.game.add.sprite(190, 292, 'check');
            this.soundeffect_check.anchor.setTo(0.5,0.5);

            if(game.global.sound_effect == 1)
                this.soundeffect_check.alpha = 1;
            else
                this.soundeffect_check.alpha = 0;

            this.soundeffect = this.game.add.sprite(185, 295, 'checkbox');
            this.soundeffect.anchor.setTo(0.5,0.5);
            this.soundeffect.inputEnabled = true;
            this.soundeffect.events.onInputUp.add(function () {
                if(game.global.sound_effect == 1 )
                {
                    game.global.sound_effect = 0;
                    this.soundeffect_check.alpha = 0;
                }
                else
                {
                    this.soundeffect_check.alpha = 1;
                    game.global.sound_effect = 1;
                }
              
            }, this);
            this.Bgm_check = this.game.add.sprite(190, 192, 'check');
            if(game.global.bgm == 1)
                this.Bgm_check.alpha = 1;
            else
                this.Bgm_check.alpha = 0;
            this.Bgm_check.anchor.setTo(0.5,0.5);
            this.Bgm = this.game.add.sprite(185, 195, 'checkbox');
            this.Bgm.anchor.setTo(0.5,0.5);
            this.Bgm.inputEnabled = true;
            this.Bgm.events.onInputUp.add(function () {
                if(game.global.bgm ==  1)
                {
                    game.global.bgm = 0;
                    this.Bgm_check.alpha = 0;
                }
                else
                {
                    this.Bgm_check.alpha = 1;
                    game.global.bgm = 1;
                }
              
            }, this);
            close= this.game.add.sprite(455, 35, 'close');
            close.anchor.setTo(0.5,0.5);
            close.inputEnabled = true;
            close.events.onInputUp.add(function () {
                game.state.start('menu');
            }, this);

        }
        else{
            blue = game.add.sprite(0,0,'blue');
            blue.alpha = 0.99;
            close= this.game.add.sprite(455, 35, 'close');
            close.anchor.setTo(0.5,0.5);
            close.inputEnabled = true;
            close.events.onInputUp.add(function () {
                game.state.start('menu');
            }, this);
            bestscoreLabel = game.add.text(game.width/2,65,"高分排行榜",{font:'50px Noto Sans TC',fill:'#ffffff'});
            bestscoreLabel.anchor.setTo(0.5,0.5);
            //var first = game.add.text(game.width/2,200,"#1  "+ "---" + "      " + "--" ,{font:'30px Arial',fill:'#ffffff'}).anchor.setTo(0.5,0.5);
            //var second = game.add.text(game.width/2,250,"#2  "+ "---" + "      " + "--" ,{font:'30px Arial',fill:'#ffffff'}).anchor.setTo(0.5,0.5);
            //var third = game.add.text(game.width/2,300,"#3  "+ "---" + "      " + "--" ,{font:'30px Arial',fill:'#ffffff'}).anchor.setTo(0.5,0.5);
            //var forth = game.add.text(game.width/2,350,"#4  "+ "---" + "      " + "--" ,{font:'30px Arial',fill:'#ffffff'}).anchor.setTo(0.5,0.5);
            //var fifth = game.add.text(game.width/2,400,"#5  "+ "---" + "      " + "--" ,{font:'30px Arial',fill:'#ffffff'}).anchor.setTo(0.5,0.5);

            var database = firebase.database().ref('score_list').orderByChild('score').limitToLast(5);
            i = 4;
            database.once('value', function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    // console.log(childSnapshot.val().name);
                    // console.log(childSnapshot.val().score);
                    var x = childSnapshot.val().name;
                    var y = childSnapshot.val().score;
                    // Name.push(x);
                    
                    // Score.push(y);
                    if(i==0){
                        //console.log("i = " + i );
                        //console.log("x = " + x );
                        //console.log("y = " + y );
                        //console.log("1");
                        //first.text = "#1  "+ x + "      " + y;
                        game.add.text(150,200,"#1  "+ x ,{font:'30px Noto Sans TC',fill:'#ffffff'});
                        game.add.text(300,200,y,{font:'30px Noto Sans TC',fill:'#ffffff'});
                        //console.log("1.1");
                    } 
                    else if(i==1){
                        //console.log("i = " + i );
                        //console.log("x = " + x );
                        //console.log("y = " + y );
                        //console.log("2");
                        //second.text = "#2  "+ x + "      " + y;
                        game.add.text(150,250,"#2  "+ x,{font:'30px Noto Sans TC',fill:'#ffffff'});
                        game.add.text(300,250,y,{font:'30px Noto Sans TC',fill:'#ffffff'});
                        //console.log("2.2");
                    }
                    else if(i==2){
                        //console.log("i = " + i );
                        //console.log("x = " + x );
                        //console.log("y = " + y );
                        //console.log("3");
                        //third.text = "#3  "+ x + "      " + y;
                        game.add.text(150,300,"#3  "+ x ,{font:'30px Noto Sans TC',fill:'#ffffff'});
                        game.add.text(300,300,y,{font:'30px Noto Sans TC',fill:'#ffffff'});
                        //console.log("3.3");
                    }
                    else if(i==3){
                        //console.log("i = " + i );
                        //console.log("x = " + x );
                        //console.log("y = " + y );
                        //console.log("4");
                        //forth.text = "#4  "+ x + "      " + y;
                        game.add.text(150,350,"#4  "+ x,{font:'30px Noto Sans TC',fill:'#ffffff'});
                        game.add.text(300,350,y,{font:'30px Noto Sans TC',fill:'#ffffff'});
                        //console.log("4.4");
                    }
                    else if(i==4){
                        //console.log("i = " + i );
                        //console.log("x = " + x );
                        //console.log("y = " + y );
                        //console.log("5");
                        //fifth.text = "#5  "+ x + "      " + y;
                        game.add.text(150,400,"#5  "+ x ,{font:'30px Noto Sans TC',fill:'#ffffff'});
                        game.add.text(300,400,y,{font:'30px Noto Sans TC',fill:'#ffffff'});
                        //console.log("5.5");
                    };

                    i--;
                })
            });
       

        }

    }
}