var loadState = {
    preload: function(){

        //Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width/2,150,'loading...',{font:'30px Arial',fill:'#ffffff'});
        loadingLabel.anchor.setTo(0.5,0.5);

        //Display the progress bar
        var progressBar = game.add.sprite(game.width/2,200,'ProgressBar');
        progressBar.anchor.setTo(0.5,0.5);
        game.load.setPreloadSprite(progressBar);

        //Load all game assets
        game.load.image('background','assets/background.png');
        game.load.spritesheet('player','assets/airplane.png',200,200);
        game.load.spritesheet('player2','assets/airplane2.png',200,200);
        game.load.image('bullet','assets/bullet.png');
        game.load.image('bullet2','assets/bullet2.png');
        game.load.image('enemy','assets/enemy.png');
        game.load.image('enemyBullet','assets/enemy_bullet.png');
        game.load.image('pause','assets/pause.png');
        game.load.image('continue','assets/continue.png');
        game.load.image('white','assets/white.jpg');
        game.load.image('menu-background','assets/menu-background.jpg');
        game.load.image('blue','assets/blue.png');
        game.load.image('pixel1','assets/pixel1.png');
        game.load.image('pixel2','assets/pixel2.png');
        game.load.image('pixel3','assets/pixel3.png');
        game.load.image('pixel4','assets/pixel4.png');
        game.load.audio('bgm',['assets/bgm.ogg','assets/bgm.mp3']);
        game.load.audio('bgm2',['assets/bgm2.ogg','assets/bgm2.mp3']);
        game.load.audio('player_shoot',['assets/player_shoot.ogg','assets/player_shoot.mp3']);
        game.load.audio('playerhitenemy',['assets/player_hit_enemy.ogg','assets/player_hit_enemy.mp3']);
        game.load.audio('playerhitbullet',['assets/minus_life.ogg','assets/minus_life.mp3']);
        game.load.audio('max',['assets/max.ogg','assets/max.mp3']);
        game.load.audio('ultimate_sound',['assets/ultimate_sound.ogg','assets/max.mp3']);
        game.load.audio('eat_magic_ball_sound',['assets/eat_magic_ball.ogg','assets/eat_magic_ball.mp3']);
        game.load.image('add','assets/add.png');
        game.load.image('subtract','assets/subtract.png');
        game.load.image('black','assets/black.png');
        game.load.image('checkbox','assets/checkbox.png');
        game.load.image('check','assets/check.png');
        game.load.image('close','assets/close.png');
        game.load.image('ultimate','assets/ultimate.png');
        game.load.image('ultimate2','assets/ultimate2.png');
        game.load.image('blue','assets/blue.png');
        game.load.image('unique_bullet','assets/unique_bullet.png'); 
        game.load.image('magic_ball','assets/magic_ball.png'); 
        
      
    },
    create: function(){
        //Go to the menu state
        game.state.start('menu');
    }
}