//Create the States
var playState2 = {
    preload: function(){
        
    },
    create: function(){
        this.backgound = game.add.tileSprite(0, 0, 500, 600, 'background');
        this.game.paused = true;
        this.gameover = 0;

        game.global.score = 0;
        game.global.score2 = 0;

        this.player = game.add.sprite(400,game.height -50,'player'); // display player as sprite
        this.player.animations.add('toright',[13,14],8,false);
        this.player.animations.add('toleft',[11,10],8,false);
        this.player.anchor.setTo(0.5,0.5); // Anchir Point Center
        this.player.scale.setTo(0.3,0.3); // Anchir Point Center
        game.physics.arcade.enable(this.player); // Add physics to player
        this.cursor = game.input.keyboard.createCursorKeys(); //arrow keys to control player
        this.player.body.collideWorldBounds = true; //讓player不會超出遊戲邊界
        this.player_blood = 15;
	    this.playerHealthBar = new HealthBar(this.game, {width: 150, height: 15,x: 400, y: 60,bg:{color:'#D3D3D3'},bar:{color:'#ff3333'}});
        this.playerBloodText =  game.add.text(375,50,this.player_blood + ' / 15',{font: '18px Arial',fill: '#000000'});
    
        this.utimateskillbar = new HealthBar(this.game, {width: 150, height: 15,x: 400, y: 80 ,bg:{color:'#ffff00'},bar:{color:'#ff8000'}});
        this.utimateskillbar.setPercent(29*100/30);
       
        

        this.bullets = game.add.group();
        this.bullets.enableBody = true;
        this.bullets.createMultiple(30, 'bullet');
        this.bullets.setAll('anchor.x', 0.5);
        this.bullets.setAll('anchor.y', 1);
        this.bullets.setAll('scale.x', 0.5);
        this.bullets.setAll('scale.y', 0.5);
        this.bullets.setAll('outOfBoundsKill', true);
        this.bullets.setAll('checkWorldBounds', true);
        this.fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.bulletTime = 0;

        this.player2 = game.add.sprite(100,game.height -50,'player2'); // display player as sprite
        this.player2.animations.add('toright',[13,14],8,false);
        this.player2.animations.add('toleft',[11,10],8,false);
        this.player2.anchor.setTo(0.5,0.5); // Anchir Point Center
        this.player2.scale.setTo(0.3,0.3); // Anchir Point Center
        game.physics.arcade.enable(this.player2); // Add physics to player
        //this.cursor = game.input.keyboard.createCursorKeys(); //arrow keys to control player
        // Create WASD Keys
        this.wasd = {
            up: game.input.keyboard.addKey(Phaser.Keyboard.W),
            left: game.input.keyboard.addKey(Phaser.Keyboard.A),
            right: game.input.keyboard.addKey(Phaser.Keyboard.D),
            down: game.input.keyboard.addKey(Phaser.Keyboard.S)
        }
        this.player2.body.collideWorldBounds = true; //讓player不會超出遊戲邊界
        this.player_blood2 = 15;
	    this.playerHealthBar2 = new HealthBar(this.game, {width: 150, height: 15,x: 95, y: 60,bg:{color:'#D3D3D3'},bar:{color:'#ff3333'}});
        this.playerBloodText2 =  game.add.text(70,50,this.player_blood + ' / 15',{font: '18px Arial',fill: '#000000'});
    
        this.utimateskillbar2 = new HealthBar(this.game, {width: 150, height: 15,x: 95, y: 80 ,bg:{color:'#ffff00'},bar:{color:'#ff8000'}});
        this.utimateskillbar2.setPercent(29*100/30);


        this.weapon = game.add.weapon(30, 'unique_bullet');
        this.weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.weapon.bulletAngleOffset = 90;
        this.weapon.bulletSpeed = 400;
        this.weapon.fireRate = 100;
        this.weapon.bulletAngleVariance = 30;

        this.weapon2 = game.add.weapon(30, 'unique_bullet');
        this.weapon2.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.weapon2.bulletAngleOffset = 90;
        this.weapon2.bulletSpeed = 400;
        this.weapon2.fireRate = 100;
        this.weapon2.bulletAngleVariance = 30;
        

        this.magic_balls = game.add.group();
        this.magic_balls.enableBody = true;
        this.magic_balls.physicsBodyType = Phaser.Physics.ARCADE;
        this.magic_balls.createMultiple(1, 'magic_ball');
        this.magic_balls.setAll('anchor.x', 0.5);
        this.magic_balls.setAll('anchor.y', 1);
        this.magic_balls.setAll('scale.x', 0.07);
        this.magic_balls.setAll('scale.y', 0.07);
        this.magic_balls.setAll('outOfBoundsKill', true);
        this.magic_balls.setAll('checkWorldBounds', true);

        this.which_bullet = 0;
        this.which_bullet2 = 0;

        game.time.events.loop(30000,
            function () {
                this.magic_ball = this.magic_balls.getFirstDead(); // get the first dead enemy of the group
       
                if(!this.magic_ball){return;}
        
                this.randomX = Math.floor((Math.random() * 400) + 50);
                this.magic_ball.reset(this.randomX,0);
                this.magic_ball.body.velocity.y = 100;

                
               

            }
            ,this); 

        this.eat_magic_ball_sound = game.add.audio('eat_magic_ball_sound');
        if(game.global.bgm == 1){
            this.eat_magic_ball_sound.volume = game.global.volume;
        }
        else
        {
            this.eat_magic_ball_sound.volume = 0;
        }
      
       
        

        this.bullets2 = game.add.group();
        this.bullets2.enableBody = true;
        this.bullets2.createMultiple(30, 'bullet2');
        this.bullets2.setAll('anchor.x', 0.5);
        this.bullets2.setAll('anchor.y', 1);
        this.bullets2.setAll('scale.x', 0.5);
        this.bullets2.setAll('scale.y', 0.5);
        this.bullets2.setAll('outOfBoundsKill', true);
        this.bullets2.setAll('checkWorldBounds', true);
        this.fireButton2 = game.input.keyboard.addKey(Phaser.Keyboard.SHIFT);
        this.bulletTime2 = 0;

        this.enemies = game.add.group(); // create an enemy group with Arcade physics
        this.enemies.enableBody = true;
        this.enemies.createMultiple(10,'enemy'); // create 10 enemies in the group
        this.enemies.setAll('anchor.x', 0.5);
        this.enemies.setAll('anchor.y', 1);
        this.enemies.setAll('scale.x', 0.1);
        this.enemies.setAll('scale.y', 0.1);
        this.enemies.setAll('outOfBoundsKill', true);
        this.enemies.setAll('checkWorldBounds', true);
        game.time.events.loop(1000,this.addEnemy,this); //to enable enemies every few seconds
        

        this.enemyBullets = game.add.group();
        this.enemyBullets.enableBody = true;
        this.enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemyBullets.createMultiple(30, 'enemyBullet');
        this.enemyBullets.setAll('anchor.x', 0.5);
        this.enemyBullets.setAll('anchor.y', 1);
        this.enemyBullets.setAll('scale.x', 0.5);
        this.enemyBullets.setAll('scale.y', 0.5);
        this.enemyBullets.setAll('outOfBoundsKill', true);
        this.enemyBullets.setAll('checkWorldBounds', true);
        this.firingTimer = 0;

        this.scoreLabel =  game.add.text(390,15,'score: 0',{font: '25px Arial',fill: '#ffffff'});
        this.scoreLabel2 =  game.add.text(20,15,'score: 0',{font: '25px Arial',fill: '#ffffff'});

        this.pause = this.game.add.sprite(250, 30, 'pause');
        this.pause.anchor.setTo(0.5,0.5);
        this.pause.scale.setTo(0.8,0.8);
        this.pause.inputEnabled = true;
        this.pause.events.onInputUp.add(function () {
            if(!this.game.paused){
                this.game.paused = true;
                //white = game.add.sprite(0,0,'white');
                this.pause.loadTexture("continue");
                var style = {fill : '#FFF',font:'50px'}
                //white.alpha = 0.5;
                text = game.add.text(game.width * 0.5, game.height * 0.5, "Pause", style);
                text.anchor.set(0.5, 0.5);
            }
            else{
                this.game.paused = false;
                this.pause.loadTexture("pause");
                text.destroy();
                //white.destroy();
            }
        }, this);

        this.bgm = game.add.audio('bgm2');
        if(game.global.bgm == 1){
            this.bgm.volume = game.global.volume;
        }
        else
        {
            this.bgm.volume = 0;
        }
        this.bgm.play();
        this.bgm.loop =true;

        this.EnterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        this.EnterKey.onDown.add(this.ultimate_skill,this);

        this.CAPS_LOCKKey = game.input.keyboard.addKey(Phaser.Keyboard.CAPS_LOCK);
        this.CAPS_LOCKKey.onDown.add(this.ultimate_skill2,this);

        this.utimate = 29;
        this.utimate2 = 29;

        
        this.max =  game.add.text(380,70,'MAX',{font: '18px Arial',fill: '#000000'});
        this.max2 =  game.add.text(75,70,'MAX',{font: '18px Arial',fill: '#000000'});
          
        this.max_sound = game.add.audio('max');
        if(game.global.sound_effect == 1){
            this.max_sound.volume = game.global.volume;
        }
        else
        {
            this.max_sound.volume = 0;
        }

        this.played = 0;
        this.played2 = 0;

        this.ultimate_sound = game.add.audio('ultimate_sound');
        if( game.global.sound_effect == 1)
            this.ultimate_sound.volume = game.global.volume;
        else
            this.ultimate_sound.volume = 0;

        this.gameTimer = 0;

        this.gameover = 0;


        this.black = game.add.sprite(0,0,'black'); 
        this.black.alpha = 0.9; 
        this.RULEText = game.add.text(250,70,"Game Rule",{font:'90px VT323',fill:'#ffffff'});
        this.RULEText.anchor.setTo(0.5,0.5);
        this.ruleText = game.add.text(250,350,"",{font:'30px VT323',fill:'#ffffff'});
        this.ruleText.text = "For Player 1:  \n● Up/Down/Left/Right keys \n   to control player\n● 'Space' key to shoot bullets\n● 'Enter' key to use ultimate skll\nFor Player 2:  \n● W/S/A/D keys to control player\n● 'Shift' key to shoot bullets\n● 'Caps lock' key to use ultimate skll\n\n● When both of the players' blood\n  become 0, game over\n● If ultimate skill bar becomes MAX,\n  then you can use ultimate skill "
        this.ruleText.anchor.setTo(0.5,0.5);
        this.close= this.game.add.sprite(455, 35, 'close');
            this.close.anchor.setTo(0.5,0.5);
            this.close.inputEnabled = true;
            this.close.events.onInputUp.add(function () {
                this.black.kill();
                this.ruleText.kill();
                this.RULEText.kill();
                this.close.kill();
                this.game.paused = false;
            }, this);
      
       
        
        

    },
    update: function(){

        if(this.player_blood <= 0 && this.player_blood2 <= 0 && this.gameover == 0 )
            this.playerDie();
        this.gameTimer += 18;
        //console.log(this.gameTimer);
        
        this.backgound.tilePosition.y += 1.5;
        game.physics.arcade.overlap(this.player,this.enemyBullets,this.player_hit_bullet,null,this); 
        game.physics.arcade.overlap(this.player2,this.enemyBullets,this.player2_hit_bullet,null,this); 
        game.physics.arcade.overlap(this.enemies,this.bullets,this.enemy_hit_bullet,null,this); 
        game.physics.arcade.overlap(this.enemies,this.bullets2,this.enemy_hit_bullet2,null,this); 
        game.physics.arcade.overlap(this.player,this.enemies,this.player_hit_enemy,null,this); 
        game.physics.arcade.overlap(this.player2,this.enemies,this.player2_hit_enemy,null,this); 

        game.physics.arcade.overlap(this.player,this.magic_balls,this.eat_magic_ball,null,this); 
        game.physics.arcade.overlap(this.player2,this.magic_balls,this.eat_magic_ball2,null,this);
        game.physics.arcade.overlap(this.enemies,this.weapon.bullets,this.enemy_hit_weapon,null,this);  
        game.physics.arcade.overlap(this.enemies,this.weapon2.bullets,this.enemy_hit_weapon2,null,this);
        this.movePlayer();
        if (game.time.now > this.firingTimer)
        {
            this.enemyFires();
        }

        if(this.utimate >= 30)
            this.max.alpha = 1;
        else 
            this.max.alpha = 0;
        
        if(this.utimate == 30 && this.played!=1)
        {
            //le.log("hi");
            this.played = 1;
            this.max_sound.play();
        }

        if(this.utimate2 >= 30)
            this.max2.alpha = 1;
        else 
            this.max2.alpha = 0;
        
        if(this.utimate2 == 30 && this.played2!=1)
        {
            //le.log("hi");
            this.played2 = 1;
            this.max_sound.play();
        }

        if(this.gameTimer >= 180000 ){

            this.addEnemy();
        }

    
        
        
        
    },
    movePlayer: function(){
        if(this.cursor.left.isDown ){
            this.player.body.velocity.y = 0; 
            this.player.body.velocity.x = -200;
            if(this.player.frame != 10 )
            {
                this.player.animations.play('toleft');
            }
        }
        else if(this.cursor.right.isDown ){
            this.player.body.velocity.y = 0; 
            this.player.body.velocity.x = 200;
            if(this.player.frame != 14 )
            {
                this.player.animations.play('toright');
            }
        }
        else if(this.cursor.up.isDown ){
            this.player.body.velocity.x = 0; 
            this.player.body.velocity.y = -200;
            this.player.frame = 12; 
        }
        else if(this.cursor.down.isDown ){
            this.player.body.velocity.x = 0; 
            this.player.body.velocity.y = 200;
            this.player.frame = 12; 
        }
        else{
            this.player.body.velocity.x = 0; 
            this.player.body.velocity.y = 0; 
            this.player.frame = 12; 
        }

        if (this.fireButton.isDown && this.gameover == 0)
        {
            if(this.which_bullet == 0)
                this.fireBullet();
            else{
                this.weapon.trackSprite(this.player, 14, 0);
                this.weapon.fire();
            }
            //this.fireBullet();
        }

        if(this.wasd.left.isDown ){
            this.player2.body.velocity.y = 0; 
            this.player2.body.velocity.x = -200;
            if(this.player2.frame != 10 )
            {
                this.player2.animations.play('toleft');
            }
        }
        else if(this.wasd.right.isDown ){
            this.player2.body.velocity.y = 0; 
            this.player2.body.velocity.x = 200;
            if(this.player2.frame != 14 )
            {
                this.player2.animations.play('toright');
            }
        }
        else if(this.wasd.up.isDown){
            this.player2.body.velocity.x = 0; 
            this.player2.body.velocity.y = -200;
            this.player2.frame = 12; 
        }
        else if(this.wasd.down.isDown ){
            this.player2.body.velocity.x = 0; 
            this.player2.body.velocity.y = 200;
            this.player2.frame = 12; 
        }
        else{
            this.player2.body.velocity.x = 0; 
            this.player2.body.velocity.y = 0; 
            this.player2.frame = 12; 
        }

        if (this.fireButton2.isDown && this.gameover == 0)
        {
            if(this.which_bullet2 == 0)
                this.fireBullet2();
            else{
                this.weapon2.trackSprite(this.player2, 14, 0);
                this.weapon2.fire();
            }
            //this.fireBullet2();
        }

    },
    eat_magic_ball: function(player,magic_ball){
        this.eat_magic_ball_sound.play();
        this.magic_ball.kill();
        this.which_bullet = 1;
        game.time.events.add(
            10000, // Start callback after 1000ms
            function(){
                this.which_bullet = 0;
            }, // Callback
            this
        ); 


    },
    eat_magic_ball2: function(player,magic_ball){
        this.eat_magic_ball_sound.play();
        this.magic_ball.kill();
        this.which_bullet2 = 1;
        game.time.events.add(
            10000, // Start callback after 1000ms
            function(){
                this.which_bullet2 = 0;
            }, // Callback
            this
        ); 
    },
    fireBullet: function(){
        

        //  To avoid them being allowed to fire too fast we set a time limit
        if (game.time.now > this.bulletTime)
        {
            this.player_shoot = game.add.audio('player_shoot');
            if(game.global.sound_effect == 1){
                this.player_shoot.volume = game.global.volume;
            }
            else{
                this.player_shoot.volume = 0;
            }
            this.player_shoot.play();
             
            //  Grab the first bullet we can from the pool
            this.bullet = this.bullets.getFirstExists(false);

            if (this.bullet)
            {
                //  And fire it
                this.bullet.reset(this.player.x, this.player.y + 8);
                this.bullet.body.velocity.y = -400;
                this.bulletTime = game.time.now + 150;
            }
        }
    },
    fireBullet2: function(){
        

        //  To avoid them being allowed to fire too fast we set a time limit
        if (game.time.now > this.bulletTime2)
        {
            this.player_shoot = game.add.audio('player_shoot');
            if(game.global.sound_effect == 1){
                this.player_shoot.volume = game.global.volume;
            }
            else{
                this.player_shoot.volume = 0;
            }
            this.player_shoot.play();
             
            //  Grab the first bullet we can from the pool
            this.bullet2 = this.bullets2.getFirstExists(false);

            if (this.bullet2)
            {
                //  And fire it
                this.bullet2.reset(this.player2.x, this.player2.y + 8);
                this.bullet2.body.velocity.y = -400;
                this.bulletTime2 = game.time.now + 150;
            }
        }
    },
    addEnemy: function(){
        this.enemy = this.enemies.getFirstDead(); // get the first dead enemy of the group
        // if there isn't any dead enemy, do nothing
        if(!this.enemy){return;}
        /* Initialize enemy */
        this.randomX = Math.floor((Math.random() * 400) + 50);
        this.enemy.reset(this.randomX,0); 
        if(this.gameTimer >= 240000)
            this.enemy.body.velocity.y = 200;
        else if(this.gameTimer >= 180000 &&　this.gameTimer< 240000)
            this.enemy.body.velocity.y = 100;
        else if(this.gameTimer >= 120000 &&　this.gameTimer< 180000)
            this.enemy.body.velocity.y = 200;
        else if(this.gameTimer >= 60000 &&　this.gameTimer < 120000 )
            this.enemy.body.velocity.y = 100;
        else 
            this.enemy.body.velocity.y = 50;
        this.enemy_tween = game.add.tween(this.enemy).to({x:this.enemy.x + 20}, 1000).to({x:this.enemy.x -40}, 2000).to({x:this.enemy.x},1000).loop().start();
        //this.enemyBloodText =  game.add.text(345,18,this.player_blood + ' / 15',{font: '23px Arial',fill: '#000000'});
    
    },
    moveEnemyHeathBar: function(){
        this.enemyHealthBar.setPosition(this.enemy.x, this.enemy.y);
    },
    enemyFires: function(){
        //  Grab the first bullet we can from the pool
        this.enemyBullet = this.enemyBullets.getFirstExists(false);
        var livingEnemies = [];
        livingEnemies.length = 0 ;

        this.enemies.forEachAlive(function(enemy){
            // put every living enemy in an array
            //if(enemy.x > 0)
                livingEnemies.push(enemy);
        });


        if (this.enemyBullet && livingEnemies.length > 0)
        {
            // randomly select one of them
            this.shooter = livingEnemies[game.rnd.integerInRange(0,livingEnemies.length-1)];
            // And fire the bullet from this enemy
            this.enemyBullet.reset(this.shooter.x, this.shooter.y);
            if(this.gameTimer >= 240000)
                this.enemyBullet.body.velocity.y = 300;
            else if(this.gameTimer >= 180000  &&　this.gameTimer< 240000)
                this.enemyBullet.body.velocity.y = 200;
            else if(this.gameTimer >= 120000 &&　this.gameTimer< 180000)
                this.enemyBullet.body.velocity.y = 400;
            else if(this.gameTimer >= 60000 &&　this.gameTimer < 120000 )
                this.enemyBullet.body.velocity.y = 300;
            else
                this.enemyBullet.body.velocity.y = 200;

            //game.physics.arcade.moveToObject(enemyBullet,player,120);
            if(this.gameTimer >= 180000)
                this.firingTimer = game.time.now + 400;
            else
                this.firingTimer = game.time.now + 800;
        }

    },
    enemy_hit_weapon: function(enemy,bullet)
    {
        //enemy hit player bullet
        this.emitter3 = game.add.emitter(enemy.x,enemy.y,5);
        this.emitter3.makeParticles('ultimate');
        this.emitter3.setYSpeed(150,0);
        this.emitter3.setXSpeed(-150,150);
        this.emitter3.setScale(1,0,1,0,500);
        this.emitter3.gravity = 0; 
        this.emitter3.start(true,500,null,5);
        bullet.kill(); 
        enemy.kill();
        game.time.events.add(
            1000, // Start callback after 1000ms
            function(){
                this.emitter3.kill();
            }, // Callback
            this
        );   
        
        game.global.score += 1;
        this.scoreLabel.text = 'score:' + game.global.score;
        if(this.utimate <= 30){
            this.utimate += 1;
        }
        
        if(this.utimate <= 30)
            this.utimateskillbar.setPercent((this.utimate/30)*100);
        //console.log("score = " + this.score);
    },
    enemy_hit_weapon2: function(enemy,bullet)
    {
        //enemy hit player bullet
        this.emitter3 = game.add.emitter(enemy.x,enemy.y,5);
        this.emitter3.makeParticles('ultimate');
        this.emitter3.setYSpeed(150,0);
        this.emitter3.setXSpeed(-150,150);
        this.emitter3.setScale(1,0,1,0,500);
        this.emitter3.gravity = 0; 
        this.emitter3.start(true,500,null,5);
        bullet.kill(); 
        enemy.kill();
        game.time.events.add(
            1000, // Start callback after 1000ms
            function(){
                this.emitter3.kill();
            }, // Callback
            this
        );   
        
        game.global.score2 += 1;
        this.scoreLabel2.text = 'score:' + game.global.score2;
        if(this.utimate2 <= 30){
            this.utimate2 += 1;
        }
        
        if(this.utimate2 <= 30)
            this.utimateskillbar2.setPercent((this.utimate2/30)*100);
        //console.log("score = " + this.score);
    },
    player_hit_bullet: function(player,bullet){
        this.playerhitbullet = game.add.audio('playerhitbullet');
        if( game.global.sound_effect == 1)
            this.playerhitbullet.volume = game.global.volume;
        else
            this.playerhitbullet.volume = 0;
        this.playerhitbullet.play();
        //player hit enemy bullet
        this.emitter1 = game.add.emitter(this.player.x,this.player.y,10);
        this.emitter1.makeParticles('pixel1');
        this.emitter1.setYSpeed(-150,0);
        this.emitter1.setXSpeed(-150,150);
        this.emitter1.setScale(0.4,0,0.4,0,600);
        this.emitter1.gravity = 0; 
        //this.emitter1.x = this.player.x;
        //this.emitter1.y = this.player.y;
        this.emitter1.start(true,600,null,10);
        game.time.events.add(
            1000, // Start callback after 1000ms
            function(){
                this.emitter1.kill();
                
            }, // Callback
            this
        );   
        
        bullet.kill();
        this.player_blood = this.player_blood - 1;
        if(this.player_blood <= 0)
            this.player_blood = 0;
        this.playerHealthBar.setPercent((this.player_blood/15)*100);
        this.playerBloodText.text = this.player_blood + ' / 15';
        //console.log("player blood = " + this.player_blood);
        if(this.player_blood <= 0)
        {
            this.player.kill();
            //this.playerDie();
        }
    },
    player2_hit_bullet: function(player,bullet){
        this.playerhitbullet = game.add.audio('playerhitbullet');
        if( game.global.sound_effect == 1)
            this.playerhitbullet.volume = game.global.volume;
        else
            this.playerhitbullet.volume = 0;
        this.playerhitbullet.play();
        //player hit enemy bullet
        this.emitter1 = game.add.emitter(this.player2.x,this.player2.y,10);
        this.emitter1.makeParticles('pixel1');
        this.emitter1.setYSpeed(-150,0);
        this.emitter1.setXSpeed(-150,150);
        this.emitter1.setScale(0.4,0,0.4,0,600);
        this.emitter1.gravity = 0; 
        //this.emitter1.x = this.player.x;
        //this.emitter1.y = this.player.y;
        this.emitter1.start(true,600,null,10);
        game.time.events.add(
            1000, // Start callback after 1000ms
            function(){
                this.emitter1.kill();
                
            }, // Callback
            this
        );   
        
        bullet.kill();
        this.player_blood2 = this.player_blood2 - 1;
        if(this.player_blood2 <= 0)
            this.player_blood2 = 0;
        this.playerHealthBar2.setPercent((this.player_blood2/15)*100);
        this.playerBloodText2.text = this.player_blood2 + ' / 15';
        //console.log("player blood = " + this.player_blood);
        if(this.player_blood2 <= 0)
        {
            this.player2.kill();
            //this.playerDie();
        }
    },
    enemy_hit_bullet: function(enemy,bullet)
    {
        //enemy hit player bullet
        this.emitter3 = game.add.emitter(enemy.x,enemy.y,5);
        this.emitter3.makeParticles('pixel3');
        this.emitter3.setYSpeed(150,0);
        this.emitter3.setXSpeed(-150,150);
        this.emitter3.setScale(1,0,1,0,500);
        this.emitter3.gravity = 0; 
        this.emitter3.start(true,500,null,5);
        bullet.kill();
        enemy.kill();
        game.time.events.add(
            1000, // Start callback after 1000ms
            function(){
                this.emitter3.kill();
            }, // Callback
            this
        );   
        
        game.global.score += 1;
        this.scoreLabel.text = 'score:' + game.global.score;
        if(this.utimate <= 30){
            this.utimate += 1;
        }
        
        if(this.utimate <= 30)
            this.utimateskillbar.setPercent((this.utimate/30)*100);
        //console.log("score = " + this.score);
    },
    enemy_hit_bullet2: function(enemy,bullet)
    {
        //enemy hit player bullet
        this.emitter3 = game.add.emitter(enemy.x,enemy.y,5);
        this.emitter3.makeParticles('pixel4');
        this.emitter3.setYSpeed(150,0);
        this.emitter3.setXSpeed(-150,150);
        this.emitter3.setScale(1,0,1,0,500);
        this.emitter3.gravity = 0; 
        this.emitter3.start(true,500,null,5);
        bullet.kill();
        enemy.kill();
        game.time.events.add(
            1000, // Start callback after 1000ms
            function(){
                this.emitter3.kill();
            }, // Callback
            this
        );   
        
        game.global.score2 += 1;
        this.scoreLabel2.text = 'score:' + game.global.score2;
        if(this.utimate2 <= 30){
            this.utimate2 += 1;
        }
        
        if(this.utimate2 <= 30)
            this.utimateskillbar2.setPercent((this.utimate2/30)*100);
        //console.log("score = " + this.score);
    },
    player_hit_enemy: function(player,enemy)
    {
        this.playerhitenemy = game.add.audio('playerhitenemy');
        if( game.global.sound_effect == 1)
            this.playerhitenemy.volume = game.global.volume;
        else
            this.playerhitenemy.volume = 0;
        this.playerhitenemy.play();
        //player hit enemy 
        //game.camera.shake(0.01,200);
        this.emitter2 = game.add.emitter(this.player.x,this.player.y,10);
        this.emitter2.makeParticles('pixel2');
        this.emitter2.setYSpeed(-150,0);
        this.emitter2.setXSpeed(-120,120);
        this.emitter2.setScale(1,0,1,0,800);
        this.emitter2.gravity = 0;   
        //this.emitter2.x = this.player.x;
        //this.emitter2.y = this.player.y;
        this.emitter2.start(true,800,null,10);
        game.time.events.add(
            1000, // Start callback after 1000ms
            function(){
                this.emitter2.kill();
                
            }, // Callback
            this
        );      
        enemy.kill();
        this.player_blood = this.player_blood - 5;
        if(this.player_blood <= 0)
            this.player_blood = 0;
        this.playerHealthBar.setPercent((this.player_blood/15)*100);
        this.playerBloodText.text = this.player_blood + ' / 15';
        //console.log("player blood = " + this.player_blood);
        if(this.player_blood <= 0)
        {
            this.player.kill();
            //this.playerDie();
        }

    },
    player2_hit_enemy: function(player,enemy)
    {
        this.playerhitenemy = game.add.audio('playerhitenemy');
        if( game.global.sound_effect == 1)
            this.playerhitenemy.volume = game.global.volume;
        else
            this.playerhitenemy.volume = 0;
        this.playerhitenemy.play();
        //player hit enemy 
        //game.camera.shake(0.01,200);
        this.emitter2 = game.add.emitter(this.player2.x,this.player2.y,10);
        this.emitter2.makeParticles('pixel2');
        this.emitter2.setYSpeed(-150,0);
        this.emitter2.setXSpeed(-120,120);
        this.emitter2.setScale(1,0,1,0,800);
        this.emitter2.gravity = 0;   
        //this.emitter2.x = this.player.x;
        //this.emitter2.y = this.player.y;
        this.emitter2.start(true,800,null,10);
        game.time.events.add(
            1000, // Start callback after 1000ms
            function(){
                this.emitter2.kill();
                
            }, // Callback
            this
        );      
        enemy.kill();
        this.player_blood2 = this.player_blood2 - 5;
        if(this.player_blood2 <= 0)
            this.player_blood2 = 0;
        this.playerHealthBar2.setPercent((this.player_blood2/15)*100);
        this.playerBloodText2.text = this.player_blood2 + ' / 15';
        //console.log("player blood = " + this.player_blood);
        if(this.player_blood2 <= 0)
        {
            this.player2.kill();
            //this.playerDie();
        }

    },
    playerDie: function(){ 

        this.gameover = 1;
        //Flash the color white for 300ms
       // game.camera.flash(0xffffff,300);
        
        //Camera: Shake Effect
        //Shake for 300ms with an intensity of 0.02
        
        //game.camera.shake(0.02,300);
       
        white = game.add.sprite(0,0,'white');
        white.alpha = 0.5;

        game.time.events.add(
                400, // Start callback after 1000ms
                function(){
                    //this.game.paused = true;
                    scoretext = game.add.text(game.width * 0.5, game.height * 0.1, "Player1 score: " + game.global.score, {fill : '#0000000',font:'65px VT323'});
                    scoretext.anchor.set(0.5, 0.5);
                    scoretext2 = game.add.text(game.width * 0.5, game.height * 0.2, "Player2 score: " + game.global.score2, {fill : '#0000000',font:'65px VT323'});
                    scoretext2.anchor.set(0.5, 0.5);
                    if(game.global.score > game.global.score2)
                        gameover_text = game.add.text(game.width * 0.5, game.height * 0.5, "Player1 Win!!!", {fill : '#0000000',font:'80px VT323'});
                    else if(game.global.score < game.global.score2)
                        gameover_text = game.add.text(game.width * 0.5, game.height * 0.5, "Player2 Win!!!", {fill : '#0000000',font:'80px VT323'});
                    else
                        gameover_text = game.add.text(game.width * 0.5, game.height * 0.5, "Both of you win!!!", {fill : '#0000000',font:'65px VT323'});
                    gameover_text.anchor.set(0.5, 0.5);
                    other_text = game.add.text(game.width * 0.5, game.height * 0.8, "Press ' R ' to restart\n            OR\n  Press ' Q ' to quit", {fill : '#0000000',font:'40px VT323'});
                    other_text.anchor.set(0.5, 0.5);
        
                }, // Callback
                this
            );      
        game.time.events.loop(800,
            function(){
                if(other_text.alpha == 0)
                    other_text.alpha = 1;
                else
                other_text.alpha = 0;
            }
            ,this); //to enable enemies every few seconds

        var Rkey = game.input.keyboard.addKey(Phaser.Keyboard.R);
        var Qkey = game.input.keyboard.addKey(Phaser.Keyboard.Q);
        Qkey.onDown.add(function(){
            this.bgm.destroy();
            name = prompt("請Player1輸入名字","匿名");
            //console.log("name2 = " + name);
            if(name != null){
                //console.log("name2 = " + name);
                var newpostref = firebase.database().ref('score_list').push();
                newpostref.set({
                    name: name,
                    score: game.global.score
                });

                alert('Player1的分數已上傳!    '+ " 姓名: "+ name  + " 分數: " + game.global.score);
            }
            name = prompt("請Player2輸入名字","匿名");
            //console.log("name2 = " + name);
            if(name != null){
                //console.log("name2 = " + name);
                var newpostref = firebase.database().ref('score_list').push();
                newpostref.set({
                    name: name,
                    score: game.global.score2
                });

                alert('Player2的分數已上傳!    '+ " 姓名: "+ name  + " 分數: " + game.global.score2);
            }

            game.state.start('menu');
        },this);
        Rkey.onDown.add(function(){
            this.bgm.destroy();
            name = prompt("請Player1輸入名字","匿名");
            //console.log("name2 = " + name);
            if(name != null){
                //console.log("name2 = " + name);
                var newpostref = firebase.database().ref('score_list').push();
                newpostref.set({
                    name: name,
                    score: game.global.score
                });

                alert('Player1的分數已上傳!    '+ " 姓名: "+ name  + " 分數: " + game.global.score);
            }
            name = prompt("請Player2輸入名字","匿名");
            //console.log("name2 = " + name);
            if(name != null){
                //console.log("name2 = " + name);
                var newpostref = firebase.database().ref('score_list').push();
                newpostref.set({
                    name: name,
                    score: game.global.score2
                });

                alert('Player2的分數已上傳!    '+ " 姓名: "+ name  + " 分數: " + game.global.score2);
            }
            
            game.state.start('play2');
        },this);
      
        
    }, 
    ultimate_skill: function(){
        if(this.utimate >= 30){
        this.ultimate_sound.play();
        
        this.utimateskillbar.setPercent(0);
        this.played = 0;
      
        this.utimate = 0;
        this.enemies.forEachAlive(function(enemy){
            if(enemy.y >= 5){
                this.emitter4 = game.add.emitter(enemy.x,enemy.y,10);
                this.emitter4.makeParticles('pixel2');
                this.emitter4.setYSpeed(-150,0);
                this.emitter4.setXSpeed(-120,120);
                this.emitter4.setScale(1,0,1,0,800);
                this.emitter4.gravity = 0;   
                this.emitter4.start(true,800,null,10);
                game.time.events.add(
                    1000, // Start callback after 1000ms
                    function(){
                    this.emitter4.kill();
                
                    }, // Callback
                    this
                ); 
                this.emitter4 = game.add.emitter(250,300,1);
                this.emitter4.makeParticles('ultimate');
                this.emitter4.setYSpeed(0,0);
                this.emitter4.setXSpeed(0,0);
                this.emitter4.setScale(0,15,0,15,100);
                this.emitter4.gravity = 0;   
                this.emitter4.start(true,100,null,10);
                game.time.events.add(
                    1000, // Start callback after 1000ms
                    function(){
                        //console.log("hii");
                    this.emitter4.kill();
                
                    }, // Callback
                    this
                ); 
                enemy.kill();
                game.global.score += 1;
            }
        });
        this.scoreLabel.text = 'score:' + game.global.score;
    }
    },
    ultimate_skill2: function(){
        if(this.utimate2 >= 30){
        this.ultimate_sound.play();
        
        this.utimateskillbar2.setPercent(0);
        this.played2 = 0;
      
        this.utimate2 = 0;
        this.enemies.forEachAlive(function(enemy){
            if(enemy.y >= 5){
                this.emitter4 = game.add.emitter(enemy.x,enemy.y,10);
                this.emitter4.makeParticles('pixel2');
                this.emitter4.setYSpeed(-150,0);
                this.emitter4.setXSpeed(-120,120);
                this.emitter4.setScale(1,0,1,0,800);
                this.emitter4.gravity = 0;   
                this.emitter4.start(true,800,null,10);
                game.time.events.add(
                    1000, // Start callback after 1000ms
                    function(){
                    this.emitter4.kill();
                
                    }, // Callback
                    this
                ); 
                this.emitter4 = game.add.emitter(250,300,1);
                this.emitter4.makeParticles('ultimate2');
                this.emitter4.setYSpeed(0,0);
                this.emitter4.setXSpeed(0,0);
                this.emitter4.setScale(0,15,0,15,100);
                this.emitter4.gravity = 0;   
                this.emitter4.start(true,100,null,10);
                game.time.events.add(
                    1000, // Start callback after 1000ms
                    function(){
                        //console.log("hii");
                    this.emitter4.kill();
                
                    }, // Callback
                    this
                ); 
                enemy.kill();
                game.global.score2 += 1;
            }
        });
        this.scoreLabel2.text = 'score:' + game.global.score2;
    }
    }
    
    

};


