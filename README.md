# Software Studio 2018 Spring Assignment 02 Raiden


## What I have done
### Complete game process : 15% ✔
start menu => game view => game over => quit or play again
### Basic rules : 20% ✔
Follow the basic rules of Raiden
* player : can move, shoot and it's life will decrease when touch enemy's bullets
* Enemy : system should generate enemy and each enemy can move and attack
* Map : background will move through the game
### Jucify mechanisms : 15% ✔
Level : level will become harder or habe different levels to play <br>
Skill : player can use ultimate skills to attack enemy
### Animations : 10% ✔
Add animation to player
### Particle Systems: 10% ✔
Add particle systems to player's and enemy's bullets
### Sound effects : 5% ✔
At least two kinds of sound effect <br>
Can change volume <br>
Ex : shoot sound effect, bgm...
### UI : 5% ✔
Player health, Ultimate skill number or power, Score, volume control and Game pause
### Leaderboard : 5% ✔
Store player's name and score in firebase real-time database, and design a way to show a leaderboard to your game
### Bonus : 
Multi-player game
* Off-line : 5% ✔
* Unique bullet : 5% ✔

## Detail
1. Game menu to choose "Play 1P", "Play 2P", "Setting", "Leader board"
2. Use UP/DOWN keys to choose which option, and use ENTER key to choose
3. If choose "Setting", you will enter Volume control UI, then you can adjust BGM, Sound Effect and volume
4. If choose "Leader Board", you will enter "高分排行榜", 會顯示有前五名的玩家名字和分數 (用firebase 實作)
5. If choose "Play 1P", you will first see game rule, and then start game
6. 左上角有暫停鍵，點擊的話可遊戲暫停/繼續
7. 右上角的上面是blood bar，玩家每被子彈打到一次會扣1，每被敵機撞到一次會扣5，變0的時候會Game Over
8. 右上角的下面是ultimate skill bar, 當MAX的時候按Enter鍵可使用ultimate skill，會消滅所有的敵機(<b>Jucify mechanisms</b>)
9. 隨著遊戲時間的增加，敵機和子彈的速度會越來越快，然後敵機也會越來越多(<b>Jucify mechanisms</b>)
10. 子彈打到敵人、玩家都會有particles產生
11. 背景的MAP會捲動
12. 如果吃到magic ball，會有10秒的時間有另一種子彈軌跡模式(<b>Bonus: Unique bullet</b>)
13. If choose "Play 2P", 就可以兩個人一起玩。基本上都跟Player 1P 玩法一樣。(<b>Bonus: Off-line multi-player game</b>)
14. 

